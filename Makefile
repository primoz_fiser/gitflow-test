OUTPUT  = hello_world
CC      = gcc
CFLAGS  = -g -Wall

default: all

all: hello_world

hello_world: src/hello_world.c
	$(CC) $(CFLAGS) -o $(OUTPUT) src/hello_world.c

clean cleanall:
	$(RM) *$(OUTPUT)